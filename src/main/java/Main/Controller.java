package Main;

import Main.Go.Board;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/game")
public class Controller {

    Service service;

    public Controller(Service service) {
        this.service = service;
    }

    @PostMapping
    public void startGame(@RequestParam("n") int n, @RequestParam("m") int m) {
        service.startGame(m, n);
    }

    @GetMapping("/board")
    public Board getBoard() {
        return service.getBoard();
    }

    @GetMapping("/size")
    public int[] getSize() {
        return service.getSize();
    }

    @GetMapping("/color")
    public String getColor() {
        return service.getTurn();
    }

    @PutMapping("/move/{move}")
    public void move(@PathVariable String move) {
        service.makeMove(move);
    }

    @PutMapping("/reset")
    public void reset() {
        service.resetBoard();
    }

    @PutMapping("/rollback/{turns}")
    public void rollback(@PathVariable int turns) {
        service.rollback(turns);
    }
}