package Main;

import Main.Go.Board;
import Main.Go.Game;

import java.util.Objects;

@org.springframework.stereotype.Service
public class Service {
    private Game game;

    public Board getBoard() {
        return game.getBoard();
    }

    public Game getGame() {
        return game;
    }

    public void startGame(int m, int n) {
        game = new Game(n, m);
    }

    public void makeMove(String field) {
        if (Objects.equals(field, "pass")) {
            game.passTurn();
        } else {
            game.move(field);
        }
    }

    public void resetBoard() {
        game.reset();
    }

    public void rollback(int turns) {
        game.rollback(turns);
    }

    public int[] getSize() {
        return game.getSize();
    }

    public String getTurn() {
        return game.getTurn();
    }
}
