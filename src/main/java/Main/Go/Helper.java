package Main.Go;

import java.util.ArrayList;
import java.util.List;

public class Helper {
    public final static String alphabet = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
    /**
     * Board states after each move
     */
    private static List<Board> gameData = new ArrayList<>();

    public static void pushPosition(Board board) {
        gameData.add(new Board(board));
    }

    public static Board getPosition(int turn) {
        return new Board(gameData.get(turn));
    }

    public static Board rollback(int turn) {
        gameData = gameData.subList(0, turn + 1);
        return getPosition(turn);
    }
}
