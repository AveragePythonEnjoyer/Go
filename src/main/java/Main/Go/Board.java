package Main.Go;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Board {
    /**
     * Current position on the board
     */
    private final char[][] map;

    /**
     * Number of rows
     */
    private final int n;

    /**
     * Number of columns
     */
    private final int m;

    /**
     * Creates a 1x1 <code>Board</code>
     */
    public Board() {
        this(1, 1);
    }

    /**
     * Creates a square <code>Board</code> of size n
     */
    public Board(int n) {
        this(n, n);
    }

    /**
     * Deep copy of a board
     * @param board <code>Board</code> which should be copied
     */
    public Board(Board board){
        this.n = board.n;
        this.m = board.m;
        map = new char[n][m];
        for (int i = 0; i < n; i++){
            System.arraycopy(board.map[i], 0, map[i], 0, m);
        }
    }

    /**
     * Creates an empty <code>Board</code> with given size
     */
    public Board(int n, int m) {
        if (n > 25 || n < 1 || m < 1 || m > 25) {
            throw new IndexOutOfBoundsException("Wrong board size");
        }
        this.m = m;
        this.n = n;
        map = new char[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                map[i][j] = Color.EMPTY.getC();
            }
        }
    }

    /**
     * Makes a move on the board
     * @param move <code>Move</code>, coordinates of the move
     * @param color - <code>Color</code> of the player who made the move
     */
    public void makeMove(Move move, Color color) {
        map[move.getX()][move.getY()] = color.getC();
    }

    /**
     * Checks the board for the possible captures
     * @param move the last <code>Move</code> that was done on the board
     * @param color the <code>Color</code> of the player who made the move
     * @param bannedColor the <code>Color</code> of another player
     * @return <code>Set</code> of squares, stones in which should be removed
     */
    public Set<Move> checkLiberties(Move move, Color color, Color bannedColor){
        Set<Move> toDelete = new HashSet<>();
        this.makeMove(move, color);
        char character = bannedColor.getC();
        boolean[][] visited = new boolean[n][m];
        if (!checkLiberty(new Move(move.getX() + 1, move.getY()), character, visited)){
            addToSet(visited, toDelete);
        }
        visited = new boolean[n][m];
        if (!checkLiberty(new Move(move.getX() - 1, move.getY()), character, visited)){
            addToSet(visited, toDelete);
        }
        visited = new boolean[n][m];
        if (!checkLiberty(new Move(move.getX(), move.getY() + 1), character, visited)){
            addToSet(visited, toDelete);
        }
        visited = new boolean[n][m];
        if (!checkLiberty(new Move(move.getX(), move.getY() - 1), character, visited)){
            addToSet(visited, toDelete);
        }
        this.makeMove(move, Color.EMPTY);
        return toDelete;
    }

    /**
     * Helper function for <code>checkLiberties</code>
     * @param move coordinates of the cell where the check starts
     * @param c <code>char</code> which marks an opponent's stone
     * @param visited <code>boolean[][]</code> which contains info about already visited cells
     * @return <code>true</code> if this group has liberties <code>false</code> otherwise
     */
    private boolean checkLiberty(Move move, char c, boolean[][] visited){
        boolean free = false;
        if (!validateMove(move)){
            return false;
        }
        if (map[move.getX()][move.getY()] == c){
            return false;
        }
        if (map[move.getX()][move.getY()] == Color.EMPTY.getC()){
            return true;
        }
        visited[move.getX()][move.getY()] = true;
        if (move.getX() + 1 < n && !visited[move.getX() + 1][move.getY()]){
            free = checkLiberty(new Move(move.getX() + 1, move.getY()), c, visited);
        }
        if (move.getY() + 1 < m && !visited[move.getX()][move.getY() + 1]){
            free |= checkLiberty(new Move(move.getX(), move.getY() + 1), c, visited);
        }
        if (move.getY() - 1 >= 0 && !visited[move.getX()][move.getY() - 1]){
            free |= checkLiberty(new Move(move.getX(), move.getY() - 1), c, visited);
        }
        if (move.getX() - 1 >= 0 && !visited[move.getX() - 1][move.getY()]){
            free |= checkLiberty(new Move(move.getX() - 1, move.getY()), c, visited);
        }
        return free;
    }

    /**
     * Receives a matrix of booleans and adds coordinates of all cells which contain <code>True</code> to the set
     * @param arr - <code>boolean[][]</code>
     * @param set <code>Set</code> of type <code>Move</code> to which moves will be added
     */
    private void addToSet(boolean[][] arr, Set<Move> set){
        for (int i = 0; i < n; i++){
            for (int j =0; j < m; j++){
                if (arr[i][j]){
                    set.add(new Move(i, j));
                }
            }
        }
    }

    /**
     * Performs the captures
     * @param set The <code>Set</code> of type <code>Move</code> to be removed
     */
    public void remove(Set<Move> set){
        for(Move move : set){
            map[move.getX()][move.getY()] = Color.EMPTY.getC();
        }
    }

    /**
     * Receives a move and checks if the move target cell is located inside the board.
     * @param move The <code>Move</code> to be checked
     * @return <code>True</code> if move is valid and <code>False</code> otherwise
     */
    public boolean validateMove(Move move){
        return move.getX() < n &&
                move.getY() < m &&
                move.getX() >= 0 &&
                move.getY() >= 0;
    }

    public int getN(){
        return n;
    }

    public int getM(){
        return m;
    }

    public char[][] getMap(){
        return map;
    }

    public boolean equals(Board board){
        return Arrays.deepEquals(this.map, board.getMap());
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append((n > 10 ? "   " : "  "));
        for (int i = 0; i < m; i++) {
            s.append(Helper.alphabet.charAt(i)).append(" ");
        }
        s.delete(s.length() - 1, s.length()).append('\n');
        for (int i = 0; i < n; i++) {
            s.append(n - i).append(n > 10 ? " " : "").append(i >= n - 9 ? " " : "");
            for (int j = 0; j < m; j++) {
                s.append(map[i][j]).append(" ");
            }
            s.delete(s.length() - 1, s.length()).append('\n');
        }
        return s.toString();
    }

    public String toStr() {
        StringBuilder s = new StringBuilder();
        s.append((n > 10 ? "   " : "  "));
        for (int i = 0; i < m; i++) {
            s.append(Helper.alphabet.charAt(i)).append(" ");
        }
        s.delete(s.length() - 1, s.length()).append('\n');
        for (int i = 0; i < n; i++) {
            s.append(n - i).append(n > 10 ? " " : "").append(i >= n - 9 ? " " : "");
            for (int j = 0; j < m; j++) {
                s.append(map[i][j]).append(" ");
            }
            s.delete(s.length() - 1, s.length()).append('\n');
        }
        return s.toString();
    }
}
