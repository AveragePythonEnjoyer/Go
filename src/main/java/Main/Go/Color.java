package Main.Go;
public enum Color {
    BLACK('X'), WHITE('O'), EMPTY('.');
    private final char c;

    Color(char x) {
        this.c = x;
    }

    public char getC(){
        return c;
    }

    public Color getOpposite(){
        return this == BLACK ? WHITE : BLACK;
    }
}

