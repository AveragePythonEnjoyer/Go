package Main.Go;

public record Move(int x, int y) {

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
