package Main.Go;

public class Game {
    /**
     * Number of turn
     */
    private int turn;
    /**
     * Current state of board
     */
    private Board board;
    /**
     * Color of the player who is to move
     */
    private Color playerColor;

    /**
     * Creates a new game
     * @param n number of the rows in game board
     * @param m number of the columns in game board
     */
    public Game(int n, int m) {
        board = new Board(n, m);
        turn = 0;
        Helper.pushPosition(board);
        System.out.println(board);
    }

    /**
     * Validates the move and makes it on the board
     * @param square coordinates of the move in the Go notation
     */
    public void move(String square) {
        if (turn % 2 == 0) {
            playerColor = Color.BLACK;
        } else {
            playerColor = Color.WHITE;
        }

        Move move = translate(square);

        if (validateMove(move) && (turn <= 2 || KORule(move, playerColor))) {
            board.remove(board.checkLiberties(move, playerColor, playerColor));
            board.makeMove(move, playerColor);
            System.out.println(board);
            Helper.pushPosition(board);
            turn++;
        } else {
            System.out.println("Invalid move! Try again.\n");
        }
    }

    /**
     * Performs a full validation of the move
     * @param move coordinates of the move
     * @return <code>true</code> if move is valid <code>false</code> otherwise
     */
    private boolean validateMove(Move move) {
        return board.validateMove(move) &&
                board.getMap()[move.getX()][move.getY()] == Color.EMPTY.getC() &&
                board.checkLiberties(move, playerColor, playerColor.getOpposite()).isEmpty();
    }

    /**
     * Implementation of KO rule
     * @param move move, which is being validated
     * @param color colour of a player who made a move
     * @return <code>true</code> if move doesn't break the KO Rule <code>false</code> otherwise
     */
    private boolean KORule(Move move, Color color){
        Board temp = new Board(board);
        temp.remove(temp.checkLiberties(move, playerColor, playerColor));
        temp.makeMove(move, color);
        return !temp.equals(Helper.getPosition(turn - 1));
    }

    /**
     * Rollbacks game for n moves
     * @param n number of moves to rollback
     */
    public void rollback(int n){
        if (n > turn){
            System.out.println("Incorrect amount of moves, can't rollback this far!");
        } else {
            board = Helper.rollback(turn - n);
            turn -= n;
            System.out.println(board);
        }
    }

    /**
     * Resets the board
     */
    public void reset() {
        board = new Board(board.getN(), board.getM());
        Helper.rollback(0);
        turn = 0;
        System.out.println(board);
    }

    /**
     * Passes the turn for player who is to move
     */
    public void passTurn() {
        turn += 1;
        Helper.pushPosition(board);
    }

    /**
     * Translates the move from Go notation to object of class <code>Move</code>
     * @param square coordinates in Go notation
     * @return coordinates as an object of class move
     */
    private Move translate(String square) {
        int m = Helper.alphabet.indexOf(square.charAt(square.length() - 1));
        int n = Integer.parseInt(square.substring(0, square.length() - 1));
        return new Move(board.getN() - n, m);
    }

    public int[] getSize() {
        return new int[]{board.getN(), board.getM()};
    }

    public String getTurn(){
        return turn % 2 == 0 ? "BLACK" : "WHITE";
    }

    public Board getBoard() {
        return board;
    }
}
