package reminder;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reminder.service.NoteService;
import reminder.service.NotificationService;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

    @Bean
    ApplicationRunner applicationRunner(NoteService noteService, NotificationService notificationService) {
        return args -> {

        };
    }
}
